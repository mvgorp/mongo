<?php

namespace mongo\Http\Controllers;

use Illuminate\Http\Request;
use mongo\Issue;

class MongoController extends Controller
{
    //
    public function index(Issue $issue)
    {
        $data = [];

        $data = $issue->where('debtor','=', 30862 )->max('scandate')->toDateTime()->format("YmdHis");

        return response()->json($data);
    }
    
    public function fill()
    {

        /*
         * Delete all
         */
        collect(Issue::all())->each(function ($issue) {
            $issue->delete();
        });

        /*
         * Add first few
         */
        $names = collect(factory(Issue::class, 3)->create())->transform(function ($issue) {
            return 'Issue '.$issue['name'];
        });

        /**
         * Add more, with same names
         */
        collect(range(1,10000))->each(function() use ($names) {
            return factory(Issue::class)->create([ 'name' => $names->random() ]);
        });

        //
        return response()->json(['done']);

    }
}
