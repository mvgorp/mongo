<?php

namespace mongo;

use Jenssegers\Mongodb\Eloquent\Model as Moloquent;

/**
 * Class Issue
 * @package mongo
 */
class Issue extends Moloquent {

    /**
     * @var string
     */
    protected $collection = 'issues';

    /**
     * @var string
     */
    protected $primaryKey = '_id';

    /**
     * @var string
     */
    protected $connection = 'mongodb';

    /**
     * @var array
     */
    protected $dates = ['scandate'];
}