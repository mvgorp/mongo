<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(mongo\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(mongo\Issue::class, function (Faker\Generator $faker) {
    return [
        'scandate' => $faker->dateTimeBetween('-3 months'),
        'debtor' => $faker->randomElement(range(30000,31000)),
        'ipv4' => $faker->ipv4,
        'name' => $faker->sentence(),
        'desc' => $faker->paragraph(),
        'risk' => $faker->randomElement(['Critical', 'High', 'Medium', 'Low']),
    ];
});